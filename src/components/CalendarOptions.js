import React from 'react';

export const monthOptions = [
	{ key: 1, value: '01', text: 'Januar' },
	{ key: 2, value: '02', text: 'Februar' },
	{ key: 3, value: '03', text: 'Marec' },
	{ key: 4, value: '04', text: 'April' },
	{ key: 5, value: '05', text: 'Maj' },
	{ key: 6, value: '06', text: 'Junij' },
	{ key: 7, value: '07', text: 'Julij' },
	{ key: 8, value: '08', text: 'Avgust' },
	{ key: 9, value: '09', text: 'September' },
	{ key: 10, value: '10', text: 'Oktober' },
	{ key: 11, value: '11', text: 'November' },
	{ key: 12, value: '12', text: 'December' }
]

export const dayOptions = ['Pon.','Tor.','Sre.','Čet.','Pet.','Sob.','Ned.'];


const CalendarOptions = ({date, month, year, onChange}) => 

    <div className='calendar-options'>
        <SelectDate value={date} onChange={onChange} />
        <div className='calendar-year-month'>
            <SelectYear value={year} onChange={onChange} />
            <SelectMonth value={month} onChange={onChange} />
        </div>
    </div>


const SelectMonth = ({value, onChange}) => 
    <select data-date="month" value={value} onChange={onChange}>
        <option value="" disabled>Mesec</option>
        {
            monthOptions.map(mo => 
                <option key={mo.key} value={mo.value}>{mo.text}</option>
            )
        }
    </select>

const SelectYear = ({value, onChange}) => 
    <input value={value} type='number' placeholder='Leto' data-date="year" min={1900} max={2080} onChange={onChange} />

const SelectDate = ({value, onChange}) => 
    <input id='date-input' value={value} type="date" data-date="date" onChange={onChange} />

export default CalendarOptions

