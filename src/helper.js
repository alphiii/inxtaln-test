// Parse CSV string to JSON
export const csvJSON = csv => {
    const lines=csv.split("\n");
    const result = [];
    const headers=lines[0].split(",").map(h => h.trim());
    
    for(let i=1;i<lines.length;i++){
        
        let obj = {};
        const currentline=lines[i].split(",").map(h => h.trim());
        
        for(let j=0;j<headers.length;j++){
            if (headers[j] === 'date') {
                obj[headers[j]] = new Date (currentline[j]);
            }
            else if (headers[j] === 'repeat') {
                obj[headers[j]] = currentline[j] === 'true';
            }
            
        }
        
        result.push(obj);
    }
    return result
}