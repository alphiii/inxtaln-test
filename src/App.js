import React, { Component } from 'react';

import CalendarOptions, { monthOptions, dayOptions } from './components/CalendarOptions';
import {csvJSON} from './helper'

import './App.css';
import praznikiPath from './prazniki.csv'

const initialState = 
{
  date: '',
  month: '',
  year: '',
  calendar: []
}

class App extends Component {
  constructor () {
    super()
    this.state = {...initialState, holidays: []}
  }

  componentDidMount () {
    fetch(praznikiPath)
      .then(resp => resp.text())
      .then(praznikiCSV => {
        return (this.setState({holidays: csvJSON(praznikiCSV)}))
      }
    )
      .catch(err => console.error(err))

  }

  isDayHoliday = (day, currentMonth, year) => {
    const { holidays } = this.state
    if (holidays.find(h => h.date.getDate() === day && currentMonth === h.date.getMonth() && (h.repeat || h.date.getFullYear() === year))) {
        return true
    }
    return false
  }

  generateCalendar = (year, monthString) => {
    const month = parseInt(monthString, 10)-1
    let holiday = false
    let className = ''
    let week = []
    let calendar = []
    let day = 0
    let currentMonth = month

    const date = new Date(year, month)

    // Note: Sunday is 0, Monday is 1, and so on we transform to monday 0 index
    const firstDay = date.getDay() === 0 ? 6 : date.getDay() - 1
    const numOfDays = new Date (year, month+1, 0).getDate()

    const endingDayOnPrevMonth = date.getMonth() === 0 ? 31 : new Date (date.getFullYear(), date.getMonth(), 0).getDate()

    for (let index = 0; index < 6*7; index++) {
      className = ''
      holiday = false
      if (index % 7 === 6) {
        className = 'sunday'
      } 

      if (index < firstDay) {
        day = endingDayOnPrevMonth+index-firstDay+1
        className = `not-in-this-month ${className}`
        currentMonth = month - 1
      }
      else if (index >= firstDay+numOfDays) {
        day = index-numOfDays-firstDay+1
        className = `not-in-this-month ${className}`
        currentMonth = month + 1
      }
      else {
        day = index-firstDay+1
        currentMonth = month
      }

      holiday = this.isDayHoliday(day, currentMonth, year)

      week = [...week, {key: index, day: day, className: className, holiday: holiday}]

      if (((index+1) % 7) === 0) {
        calendar.push(week)
        week = []
      }
    }

    this.setState({calendar})
  }

  handleChangedDate = (event) => {
    // DATE input
    if (event.target.dataset.date === 'date') {
      const date = new Date(event.target.value)
      // only valid dates should change selected month, year & calendar
      if(date instanceof Date && !isNaN(date.valueOf())) {
          const month = monthOptions.find(mo => mo.key === date.getMonth() + 1).value
          this.setState({month: month, year: date.getFullYear()})
          this.generateCalendar(date.getFullYear(), month)
      }
      // set to default state if invalid date
      else {
        this.setState(initialState)
      }
    }

    // YEAR input
    else if (event.target.dataset.date === 'year') {
      // we can update the state & clear state if user deletes year value
      if (this.state.month) {
        this.setState({date: `${event.target.value}-${this.state.month}-01`})
        this.generateCalendar(event.target.value, this.state.month.toString())
      }
      // year input was cleared or invalid so we set to default state 
      else {
        this.setState(initialState)
      }
    }

    // MONTH input
    else if (event.target.dataset.date === 'month' && this.state.year) {
      this.setState({date: `${this.state.year}-${event.target.value}-01`})
      this.generateCalendar(this.state.year, event.target.value)
    }

    this.setState({[event.target.dataset.date]: event.target.value})
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Ixtlan koledar</h1>
        </header>
        <CalendarOptions {...this.state} onChange={this.handleChangedDate} />
        <div className='calendar'>
        {
          // Show day names
          this.state.calendar.length > 0 && 
            <div className='week header'> {
              dayOptions.map((day, index) => <div key={index}>{day}</div>)
            }
          </div>
        }
        {
          // Show calendar day boxes
          this.state.calendar.map((week, i) => {
            return (
              <div key={i} className='week'>
              {
                week.map(day => 
                  <div key={day.key} className={day.className}>
                    <div className='calendar-day-number'>
                      {day.day}
                    </div>
                    {
                      day.holiday && <div className='holiday-marker' />
                    }
                  </div>
              )}
              </div>
              )
            })
        }
        </div>
      </div>
    );
  }
}

export default App;
